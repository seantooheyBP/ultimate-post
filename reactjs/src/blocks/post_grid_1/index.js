const { __ } = wp.i18n
const { registerBlockType } = wp.blocks;
import Edit from './Edit'
import icons from '../../helper/icons'
registerBlockType(
    'ultimate-post/post-grid-1', {
        title: 'Post Grid #1',
        icon: {src: icons.post_grid_1},
        category: 'ultimate-post',
        description: __( 'Post grid with classic style.' ),
        keywords: [ __( 'Post Grid' ), __( 'Grid View' ), __( 'Article' ), __('Post Listing') ],
        example: {
            attributes: {
                columns: {
                    lg: 2
                },
                headingShow: false,
                paginationShow: false,
                excerptLimit: 9,
                catShow: false,
                metaList: '["metaAuthor","metaDate"]',
                queryNumber: 2
            },
        },
        edit: Edit,
        save() {
            return null;
        },
    }
)