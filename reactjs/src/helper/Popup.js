const { apiFetch } = wp
const { parse } = wp.blocks
const { Component, Fragment } = wp.element

const apiEndpoint = 'https://raw.githubusercontent.com/wpxpo/ultp-api/master/layouts/';

class Popup extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            isPopup: (props.isShow || false),
            layouts: [],
            reloadId: '',
            reload: false
        }
    }

    
    fetchLayouts() {
        const $that = this
        fetch(apiEndpoint+'layouts.json')
        .then(response => response.json())
        .then((jsonData) => {
            if (jsonData) {
                $that.setState({layouts: jsonData})
            }
        })
        .catch((error) => {
            console.error(error)
        })
    }

    componentDidMount() {
        const that = this
        document.addEventListener('keydown', function(e) {
            if(e.keyCode === 27){
                document.querySelector( '.ultp-builder-modal' ).remove();
                that.setState({isPopup:false})
            }
        });
        this.fetchLayouts();
    }

    close(){
        document.querySelector( '.ultp-builder-modal' ).remove();
        this.setState({isPopup:false})
    }

    insertBlock(templateName, paid) {
        this.setState({reload:true, reloadId:templateName})
        if(!paid){
            const $that = this
            fetch(apiEndpoint+'single/'+templateName+'.json')
            .then(response => response.text())
            .then((jsonData) => {
                if (jsonData) {
                    wp.data.dispatch('core/block-editor').insertBlocks(parse(jsonData));
                    $that.close();
                    $that.setState({reload:true, reloadId:''})
                }
            })
            .catch((error) => {
                console.error(error)
            })
        }else{
            // Paid Code From Server follow remove section commit
        }
    }

    render() {
        const {isPopup, layouts} = this.state
        return (
            <Fragment>
            { isPopup &&
                <div className="ultp-popup-wrap">
                    <div className="ultp-popup-header">
                        <div className="ultp-popup-filter-title">
                            {/* <div class="ultp-popup-filter-nav">
                                <span className={'ultp-active'}>Layouts</span>
                            </div> */}
                            <div>
                                <button class="ultp-btn-close" onClick={() => this.close()} id="ultp-btn-close"><span className="dashicons dashicons-no-alt"></span></button>
                            </div>
                        </div>
                    </div>    
                    <div className="ultp-popup-content">
                        { layouts.map(data => (
                            <div className="ultp-popup-item">
                                <div class="LazyLoad">
                                    <img src={apiEndpoint+'single/'+(data.name)+'.jpg'} alt={data.title} />
                                </div>
                                <div className="ultp-popup-info">
                                    <span className="ultp-popup-title">{data.title}</span>
                                    <span className="ultp-action-btn">
                                        <button onClick={() => this.insertBlock(data.name, data.paid)} className="ultp-popup-btn"> 
                                        {(this.state.reload && this.state.reloadId == data.name) && <span class="dashicons dashicons-update rotate" />} Import</button>
                                        <a className="ultp-popup-btn" href={data.url} target="_blank">Live Preview</a>
                                    </span>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            }
        </Fragment>
        );
    }
}

export default Popup;
