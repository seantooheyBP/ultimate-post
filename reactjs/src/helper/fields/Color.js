const { Component, Fragment } = wp.element
const { ColorPicker, Dropdown } = wp.components

class Color extends Component {
	constructor(props){
		super(props)
		this.state = {
            isOpen: false,
            colorOpen: false
		}
    }
    renderColorInset(){
        const { value, onChange } = this.props;
        return (
            <Fragment>
                <div className={ 'ultp-control-button' } onClick={()=>this.setState({colorOpen: !this.state.colorOpen})} style={{background:value}}/>
                { this.state.colorOpen &&
                    <div className={'ultp-color-popup'}>
                        <ColorPicker
                            color={value?value:''}
                            onChangeComplete={color => onChange( 'rgba('+color.rgb.r+','+color.rgb.g+','+color.rgb.b+','+color.rgb.a+')' )}/>
                    </div>
                }
            </Fragment>
        )
    }
    renderColor(){
        const { value, onChange } = this.props;
        return (
            <div className={'ultp-common-popup'}>
                <ColorPicker
                    color={value?value:''}
                    onChangeComplete={color => onChange( 'rgba('+color.rgb.r+','+color.rgb.g+','+color.rgb.b+','+color.rgb.a+')' )}/>
            </div>
        )
    }
    render(){
        const { label, value, onChange, inline, disableClear, inset } = this.props;
        return(
            <div className="ultp-field-wrap ultp-field-color">
                { label &&
                    <label>{label}</label>
                }
                { inline ?
                    this.renderColor()
                    :
                    inset ?
                    this.renderColorInset()
                    :
                    <Dropdown
                        className="ultp-range-control"
                        position="top right"
                        renderToggle={({ onToggle, onClose }) => (
                            <Fragment>
                                {(value || disableClear) && 
                                    <div className={'ultp-clear dashicons dashicons-image-rotate'} onClick={() => {onClose(); onChange('') }} /> 
                                }
                                <div className={ 'ultp-control-button' } onClick={() => onToggle()} style={{background:value}} />
                            </Fragment>
                        )}
                        renderContent={() => this.renderColor()}
                    />
                }
            </div>
        )
    }
}
export default Color