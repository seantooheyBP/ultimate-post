const {__} = wp.i18n
const { Component } = wp.element
const { ToggleControl } = wp.components

class Toggle extends Component {

    render() {
        const { value, label, onChange } = this.props
        return(
            <div className="ultp-field-wrap ultp-field-toggle">
                { label && 
                    <label>{label}</label>
                }
                <div className="ultp-sub-field">
                    <ToggleControl
                        checked={ value }
                        onChange={ val => onChange( val ) } />
                </div>
            </div>
        )
    }
}
export default Toggle