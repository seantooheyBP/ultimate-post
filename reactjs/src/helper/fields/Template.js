const { Component } = wp.element

const designEndpoint = 'https://raw.githubusercontent.com/wpxpo/ultp-api/master/design/';

class Template extends Component {
    constructor() {
        super( ...arguments );
        this.state = { designList: [] };
    }

    componentWillMount(){
        const $that = this
        let blockName = this.props.store.name
        blockName = blockName.split('/')
        fetch(designEndpoint+'design.json')
            .then(response => response.json())
            .then((jsonData) => {
                if (jsonData[blockName[1]]) {
                    $that.setState({designList: jsonData[blockName[1]]})
                }
            })
            .catch((error) => {
                console.error(error)
            })
    }

    _changeDesign(source){
        const { name, clientId, attributes } = this.props.store
        const { replaceBlock } = wp.data.dispatch('core/block-editor')
        let blockName = name.split('/')

        const removeItem = ['queryNumber', 'queryType', 'queryTax', 'queryCat', 'queryTag', 'queryOrderBy', 'queryOrder', 'queryInclude', 'queryExclude', 'queryOffset', 'metaKey'];

        fetch(designEndpoint+blockName[1]+'/'+source+'.json')
            .then(response => response.text())
            .then((jsonData) => {
                let parseData = wp.blocks.parse(jsonData)
                let attr = parseData[0].attributes
                for ( let i=0; i < removeItem.length; i++ ) {
                    if(attr[removeItem[i]]){ delete attr[removeItem[i]]; }
                }
                attr = Object.assign({}, attributes,  attr)
                parseData[0].attributes = attr
                replaceBlock(clientId, parseData );
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {
        const { label, name } = this.props.store
        const { designList } = this.state
        const blockName = name.split('/')
        return(
            <div className="ultp-field-wrap ultp-field-template">
                { label && 
                    <label>{label}</label>
                }
                <div className="ultp-sub-field-template">
                    { designList.map( (data) => {
                            return (
                                <div className="ultp-field-template-item" onClick={()=>this._changeDesign(data.source)}>
                                    <img src={'https://github.com/wpxpo/ultp-api/blob/master/design/'+blockName[1]+'/'+data.source+'.jpg?raw=true'}/>
                                    <span>{data.name}</span>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}
export default Template